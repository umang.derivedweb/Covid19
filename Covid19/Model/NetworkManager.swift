//
//  NetworkManager.swift
//  Snagpay
//
//  Created by Apple on 01/03/21.
//

import UIKit
import AlamofireObjectMapper
import Alamofire

class NetworkManager: NSObject {
    
    static let shared = NetworkManager()
    
        func webserviceCallCommon(url:String, parameters:[String:Any], headers:HTTPHeaders, completion:@escaping (CommonResponse)->()) {
    
            if Reachability.isConnectedToNetwork(){
    
                Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<CommonResponse>) in
    
                    print("URL:\(url) PARAM:\(parameters)")
                    print(response.result.value as Any)
                    if let _ = response.result.value {
    
                        completion(response.result.value!)
                    } else {
    
                    }
                }
            }else{
                print("Internet Connection not Available!")
            }
    
        }
    
    func webserviceCallGetServices(url:String, headers:HTTPHeaders, completion:@escaping (ServicesResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<ServicesResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetIssues(url:String, headers:HTTPHeaders, completion:@escaping (IssuesResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<IssuesResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    
    
    func webserviceCallGetResearches(url:String, headers:HTTPHeaders, completion:@escaping (ReserchesResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<ReserchesResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetProject(url:String, headers:HTTPHeaders, completion:@escaping (ProjectResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<ProjectResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetStory(url:String, headers:HTTPHeaders, completion:@escaping (StoryResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<StoryResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetVision(url:String, headers:HTTPHeaders, completion:@escaping (VisionResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<VisionResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetAvatar(url:String, headers:HTTPHeaders, completion:@escaping (AvatarResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<AvatarResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetCharacter(url:String, headers:HTTPHeaders, completion:@escaping (CharacterResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<CharacterResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetSeries(url:String, headers:HTTPHeaders, completion:@escaping (SeriesResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<SeriesResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetVideo(url:String, headers:HTTPHeaders, completion:@escaping (VideoResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<VideoResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetVideoDetails(url:String, headers:HTTPHeaders, completion:@escaping (VideoDetailsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<VideoDetailsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetPodcast(url:String, headers:HTTPHeaders, completion:@escaping (PodcastResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<PodcastResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetLiterature(url:String, headers:HTTPHeaders, completion:@escaping (LiteratureResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<LiteratureResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetHome(url:String, headers:HTTPHeaders, completion:@escaping (HomeResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<HomeResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
    func webserviceCallGetWebviewUrl(url:String, headers:HTTPHeaders, completion:@escaping (GetWebviewUrlsResponse)->()) {
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseObject { (response: DataResponse<GetWebviewUrlsResponse>) in
                
                print("URL:\(url)")
                print(response.result.value as Any)
                if let _ = response.result.value {
                    
                    completion(response.result.value!)
                } else {
                    
                }
            }
        }else{
            print("Internet Connection not Available!")
        }
        
    }
}
