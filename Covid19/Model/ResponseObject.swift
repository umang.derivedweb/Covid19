//
//  ResponseObject.swift
//  Snagpay
//
//  Created by Apple on 01/03/21.
//

import UIKit
import ObjectMapper

class CommonResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
    }
    
}
class ServicesResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var services:ServicesData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        services <- map["services"]
    }
    
}
class IssuesResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var issues:IssuesData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        issues <- map["issues"]
    }
    
}
class ReserchesResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:ReserchesData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class ProjectResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:ProjectData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class StoryResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:StoryData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class VisionResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:VisionData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class AvatarResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:AvatarData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class CharacterResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:CharacterData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class SeriesResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:SeriesData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class VideoResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:VideoData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class VideoDetailsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:VideoListData?
    var related_videos:[VideoListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
        related_videos <- map["related_videos"]
    }
    
}
class PodcastResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:PodcastData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
class LiteratureResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var journals:JournalData?
    var articals:ArtclesData?
    var books:BooksData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        journals <- map["journals"]
        articals <- map["articals"]
        books <- map["books"]
    }
    
}
class HomeResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var featured:FeaturedData?
    var popular:PopularData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        featured <- map["featured"]
        popular <- map["popular"]
    }
    
}
class GetWebviewUrlsResponse: Mappable {
    
    var ResponseMsg:String?
    var ResponseCode:Int?
    var data:WebviewUrlData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        ResponseMsg <- map["ResponseMsg"]
        ResponseCode <- map["ResponseCode"]
        data <- map["data"]
    }
    
}
