//
//  Model.swift
//  Snagpay
//
//  Created by Apple on 01/03/21.
//

import UIKit
import ObjectMapper


class ServicesData: Mappable {
    
    var data:[ServicesListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}

class ServicesListData: Mappable {
    
    var service_id:Int?
    var title:String?
    var description:String?
    var is_featured:Int?
    var banner:String?
    var is_popular:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        service_id <- map["service_id"]
        title <- map[Helper.shared.title]
        description <- map[Helper.shared.descriptions]
        banner <- map["banner"]
        is_featured <- map["is_featured"]
        is_popular <- map["is_popular"]
    }
}

class IssuesData: Mappable {
    
    var data:[IssuesListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}
class IssuesListData: Mappable {
    
    var issue_id:Int?
    var title:String?
    var description:String?
    var is_featured:Int?
    var banner:String?
    var is_popular:Int?
    var faq:[FaqData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        issue_id <- map["issue_id"]
        title <- map[Helper.shared.title]
        description <- map[Helper.shared.descriptions]
        banner <- map["banner"]
        is_featured <- map["is_featured"]
        is_popular <- map["is_popular"]
        faq <- map["faq"]
    }
}
class FaqData: Mappable {
    
    var issue_faq_id:Int?
    var issue_id:Int?
    var question:String?
    var answer:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        issue_faq_id <- map["issue_faq_id"]
        issue_id <- map["issue_id"]
        question <- map[Helper.shared.question]
        answer <- map[Helper.shared.answer]
    }
}
class ReserchesData: Mappable {
    
    var data:[ReserchesListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}

class ReserchesListData: Mappable {
    
    var research_id:Int?
    var title:String?
    var description:String?
    var is_featured:Int?
    var banner:String?
    var is_popular:Int?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        research_id <- map["research_id"]
        title <- map[Helper.shared.title]
        description <- map[Helper.shared.descriptions]
        banner <- map["banner"]
        is_featured <- map["is_featured"]
        is_popular <- map["is_popular"]
    }
}
class ProjectData: Mappable {
    
    var data:[ProjectListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}

class ProjectListData: Mappable {
    
    var project_id:Int?
    var title:String?
    var description:String?
    var goal:String?
    var banner:String?
    var timeline:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        project_id <- map["project_id"]
        title <- map[Helper.shared.title]
        description <- map[Helper.shared.descriptions]
        banner <- map["banner"]
        goal <- map[Helper.shared.goal]
        timeline <- map["timeline"]
    }
}

class StoryData: Mappable {
    
    var founder_details_id:Int?
    var name:String?
    var profile_pic:String?
    var post_details:String?
    var details_1:String?
    var details_2:String?
    var details_3:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        founder_details_id <- map["founder_details_id"]
        name <- map[Helper.shared.name]
        profile_pic <- map["profile_pic"]
        post_details <- map[Helper.shared.post_details]
        details_1 <- map[Helper.shared.details_1]
        details_2 <- map[Helper.shared.details_2]
        details_3 <- map[Helper.shared.details_3]
    }
}
class VisionData: Mappable {
    
    var vision_id:Int?
    var title:String?
    var description:String?
    var banner:String?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        vision_id <- map["vision_id"]
        title <- map[Helper.shared.title]
        description <- map[Helper.shared.descriptions]
        banner <- map["banner"]
        
    }
}

class AvatarData: Mappable {
    
    var data:[AvatarListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}

class AvatarListData: Mappable {
    
    var avatar_id:Int?
    var title:String?
    var image:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        avatar_id <- map["avatar_id"]
        title <- map[Helper.shared.title]
        image <- map["image"]
    }
}

class CharacterData: Mappable {
    
    var data:[CharacterListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}

class CharacterListData: Mappable {
    
    var character_id:Int?
    var title:String?
    var banner:String?
    var title1:String?
    var title2:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        character_id <- map["character_id"]
        title <- map[Helper.shared.title]
        banner <- map["banner"]
        title1 <- map[Helper.shared.title1]
        title2 <- map[Helper.shared.title2]
    }
}
class SeriesData: Mappable {
    
    var data:[SeriesListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}

class SeriesListData: Mappable {
    
    var series_id:Int?
    var title:String?
    var series_videos:[SeriesVideosData]?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        series_id <- map["series_id"]
        title <- map[Helper.shared.title]
        series_videos <- map["series_videos"]
       
    }
}
class SeriesVideosData: Mappable {
    
    var series_video_id:Int?
    var series_id:Int?
    var title:String?
    var banner:String?
    var description:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        series_video_id <- map["series_video_id"]
        series_id <- map["series_id"]
        title <- map[Helper.shared.title]
        banner <- map["banner"]
        description <- map[Helper.shared.descriptions]
    }
}
class VideoData: Mappable {
    
    var data:[VideoListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}

class VideoListData: Mappable {
    
    var video_id:Int?
    var video:String?
    var title:String?
    var category:String?
    var description:String?
    var date:String?
    var banner:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        video_id <- map["video_id"]
        video <- map["video"]
        title <- map[Helper.shared.title]
        category <- map[Helper.shared.category]
        description <- map[Helper.shared.descriptions]
        date <- map["date"]
        banner <- map["banner"]
    }
}

class PodcastData: Mappable {
    
    var comocs:ComicsData?
    var videos:VideoData?
    var podcasts:PodcastsData?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        comocs <- map["comocs"]
        videos <- map["videos"]
        podcasts <- map["prodcasts"]
    }
    
}
class ComicsData: Mappable {
    
    var data:[ComicListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}

class ComicListData: Mappable {
    
    var comic_id:Int?
    var title:String?
    var banner:String?
    
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        comic_id <- map["comic_id"]
        title <- map[Helper.shared.title]
        banner <- map["banner"]
        
    }
}
class PodcastsData: Mappable {
    
    var data:[PodcastListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}

class PodcastListData: Mappable {
    
    var prodcast_id:Int?
    var title:String?
    var banner:String?
    var audio_file:String?
    var description:String?
    var date:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        prodcast_id <- map["prodcast_id"]
        title <- map[Helper.shared.title]
        banner <- map["banner"]
        audio_file <- map["audio_file"]
        description <- map[Helper.shared.descriptions]
        date <- map["date"]
    }
}

class JournalData: Mappable {
    
    var data:[JournalListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}

class JournalListData: Mappable {
    
    var journal_id:Int?
    var title:String?
    var banner:String?
    var image:String?
    var description:String?
    var description_2:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        journal_id <- map["journal_id"]
        title <- map[Helper.shared.title]
        banner <- map["banner"]
        image <- map["image"]
        description <- map[Helper.shared.descriptions]
        description_2 <- map[Helper.shared.description_2]
    }
}
class ArtclesData: Mappable {
    
    var data:[ArtclesListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}

class ArtclesListData: Mappable {
    
    var artical_id:Int?
    var title:String?
    var banner:String?
    var image:String?
    var description:String?
    var description_2:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        artical_id <- map["artical_id"]
        title <- map[Helper.shared.title]
        banner <- map["banner"]
        image <- map["image"]
        description <- map[Helper.shared.descriptions]
        description_2 <- map[Helper.shared.description_2]
    }
}
class BooksData: Mappable {
    
    var data:[BooksListData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        data <- map["data"]
        
    }
    
}

class BooksListData: Mappable {
    
    var book_id:Int?
    var title:String?
    var banner:String?
    var image:String?
    var description:String?
    var description_2:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        book_id <- map["book_id"]
        title <- map[Helper.shared.title]
        banner <- map["banner"]
        image <- map["image"]
        description <- map[Helper.shared.descriptions]
        description_2 <- map[Helper.shared.description_2]
    }
}

class FeaturedData: Mappable {
    
    var service_id:Int?
    var title:String?
    var banner:String?
    var description:String?
    var date:String?
    var banner_thumb:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        service_id <- map["service_id"]
        title <- map[Helper.shared.title]
        banner <- map["banner"]
        description <- map[Helper.shared.descriptions]
        date <- map["date"]
        banner_thumb <- map["banner_thumb"]
    }
}
class PopularData: Mappable {
    
    var data:[FeaturedData]?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        
        data <- map["data"]
    }
}

class WebviewUrlData: Mappable {
    
    var setting_id:Int?
    var product_url:String?
    var comics_url:String?
    
    required init?(map: Map){

    }
    
    func mapping(map: Map) {
        
        setting_id <- map["setting_id"]
        product_url <- map["product_url"]
        comics_url <- map["comics_url"]
    }
}
