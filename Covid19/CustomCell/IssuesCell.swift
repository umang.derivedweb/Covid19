//
//  IssuesCell.swift
//  Covid19
//
//  Created by Apple on 04/03/21.
//

import UIKit

class IssuesCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
}
