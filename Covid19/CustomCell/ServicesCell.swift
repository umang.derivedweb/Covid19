//
//  ServicesCell.swift
//  Covid19
//
//  Created by Apple on 03/03/21.
//

import UIKit

class ServicesCell: UITableViewCell {

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var btnLearnmore: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
