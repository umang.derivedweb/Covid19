//
//  VideoCollCell.swift
//  Covid19
//
//  Created by Apple on 05/03/21.
//

import UIKit

class VideoCollCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
}
