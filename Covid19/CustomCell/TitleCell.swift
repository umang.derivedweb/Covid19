//
//  TitleCell.swift
//  Covid19
//
//  Created by Apple on 04/03/21.
//

import UIKit

class TitleCell: UITableViewCell {

    @IBOutlet weak var lblQue: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var lblAns: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
