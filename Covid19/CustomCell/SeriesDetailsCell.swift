//
//  SeriesDetailsCell.swift
//  Covid19
//
//  Created by Apple on 16/03/21.
//

import UIKit

class SeriesDetailsCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
}
