//
//  ComicCell.swift
//  Covid19
//
//  Created by Apple on 05/03/21.
//

import UIKit

class ComicCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}
