//
//  AvatarCell.swift
//  Covid19
//
//  Created by Apple on 04/03/21.
//

import UIKit

class AvatarCell: UICollectionViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
}
