//
//  AppDelegate.swift
//  Covid19
//
//  Created by Apple on 02/03/21.
//

import UIKit
import CoreData
import MMDrawerController
import UserNotifications

@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {


    var window: UIWindow?
    var centerContainer: MMDrawerController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        
        if UserDefaults.standard.value(forKey: "Language") as? String != nil && UserDefaults.standard.value(forKey: "Language") as? String != "" {
            
            Helper.shared.lang = UserDefaults.standard.value(forKey: "Language") as! String
            
            if Helper.shared.lang == "fr" {
                
                Helper.shared.title = "title_fr"
                Helper.shared.descriptions = "description_fr"
                Helper.shared.description_2 = "description_2_fr"
                Helper.shared.question = "question_fr"
                Helper.shared.answer = "answer_fr"
                Helper.shared.goal = "goal_fr"
                Helper.shared.name = "name_fr"
                Helper.shared.post_details = "post_details_fr"
                Helper.shared.details_1 = "details_1_fr"
                Helper.shared.details_2 = "details_2_fr"
                Helper.shared.details_3 = "details_3_fr"
                Helper.shared.title1 = "title1_fr"
                Helper.shared.category = "category_fr"
                Helper.shared.title2 = "title2_fr"
            }
            if Helper.shared.lang == "rw-RW" {
                
                Helper.shared.title = "title_rw"
                Helper.shared.descriptions = "description_rw"
                Helper.shared.description_2 = "description_2_rw"
                Helper.shared.question = "question_rw"
                Helper.shared.answer = "answer_rw"
                Helper.shared.goal = "goal_rw"
                Helper.shared.name = "name_rw"
                Helper.shared.post_details = "post_details_rw"
                Helper.shared.details_1 = "details_1_rw"
                Helper.shared.details_2 = "details_2_rw"
                Helper.shared.details_3 = "details_3_rw"
                Helper.shared.title1 = "title1_rw"
                Helper.shared.category = "category_rw"
                Helper.shared.title2 = "title2_rw"
                
            }
            if Helper.shared.lang == "sw" {
                
                Helper.shared.title = "title_sw"
                Helper.shared.descriptions = "description_sw"
                Helper.shared.description_2 = "description_2_sw"
                Helper.shared.question = "question_sw"
                Helper.shared.answer = "answer_sw"
                Helper.shared.goal = "goal_sw"
                Helper.shared.name = "name_sw"
                Helper.shared.post_details = "tpost_details_sw"
                Helper.shared.details_1 = "details_1_sw"
                Helper.shared.details_2 = "details_2_sw"
                Helper.shared.details_3 = "details_3_sw"
                Helper.shared.title1 = "title1_sw"
                Helper.shared.category = "category_sw"
                Helper.shared.title2 = "title2_sw"
            }
        }
         
        //Notofication
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        return true
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }

        let token = tokenParts.joined()
        print("Device Token: \(token)")
        addDeviceDetails(token: token)
        //UserDefaults.standard.set(token, forKey: DEVICE_TOKEN)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {

        // If your app was running and in the foreground
        // Or
        // If your app was running or suspended in the background and the user brings it to the foreground by tapping the push notification

        print("didReceiveRemoteNotification /(userInfo)")

        
    }
    func userNotificationCenter(_: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        //        let userInfo = notification.request.content.userInfo
        let userInfo = response.notification.request.content.userInfo
        print(userInfo["payload"])
        if let dic = userInfo["payload"] as? [String:Any] {
            if let type = dic["type"] as? String {
                redirectToSpecificVC(type: type)
            }
        }
        
    }
    
    func redirectToSpecificVC(type: String) {
        
        if type == NotiType.article.rawValue {
            
            let centerViewController = UIStoryboard(name: "Literature", bundle: nil).instantiateViewController(withIdentifier: "LiteratureVC") as! LiteratureVC
            
            Helper.shared.setupMMDrawer(storyboard: "Literature", vc: "LiteratureVC", centerViewController: centerViewController)
        }
        if type == NotiType.avatar.rawValue {
            
            let centerViewController = UIStoryboard(name: "Legends", bundle: nil).instantiateViewController(withIdentifier: "AvatarVC") as! AvatarVC
            
            Helper.shared.setupMMDrawer(storyboard: "Legends", vc: "AvatarVC", centerViewController: centerViewController)
        }
        if type == NotiType.book.rawValue {
            
            let centerViewController = UIStoryboard(name: "Literature", bundle: nil).instantiateViewController(withIdentifier: "LiteratureVC") as! LiteratureVC
                    
            Helper.shared.setupMMDrawer(storyboard: "Literature", vc: "LiteratureVC", centerViewController: centerViewController)
        }
        if type == NotiType.character.rawValue {
            
            let centerViewController = UIStoryboard(name: "Legends", bundle: nil).instantiateViewController(withIdentifier: "CharacterVC") as! CharacterVC
            
            Helper.shared.setupMMDrawer(storyboard: "Legends", vc: "CharacterVC", centerViewController: centerViewController)
        }
        if type == NotiType.comic.rawValue {
            
            let centerViewController = UIStoryboard(name: "Podcast", bundle: nil).instantiateViewController(withIdentifier: "PodcastVC") as! PodcastVC
            
            Helper.shared.setupMMDrawer(storyboard: "Podcast", vc: "PodcastVC", centerViewController: centerViewController)
        }
        if type == NotiType.issue.rawValue {
            
            let centerViewController = UIStoryboard(name: "Resources", bundle: nil).instantiateViewController(withIdentifier: "IssuesVC") as! IssuesVC
            
            Helper.shared.setupMMDrawer(storyboard: "Resources", vc: "IssuesVC", centerViewController: centerViewController)
        }
        if type == NotiType.journal.rawValue {
            
            let centerViewController = UIStoryboard(name: "Literature", bundle: nil).instantiateViewController(withIdentifier: "LiteratureVC") as! LiteratureVC
            
            Helper.shared.setupMMDrawer(storyboard: "Literature", vc: "LiteratureVC", centerViewController: centerViewController)
        }
        if type == NotiType.prodcast.rawValue {
            
            let centerViewController = UIStoryboard(name: "Podcast", bundle: nil).instantiateViewController(withIdentifier: "PodcastVC") as! PodcastVC
            
            Helper.shared.setupMMDrawer(storyboard: "Podcast", vc: "PodcastVC", centerViewController: centerViewController)
        }
        if type == NotiType.project.rawValue {
            
            let centerViewController = UIStoryboard(name: "Resources", bundle: nil).instantiateViewController(withIdentifier: "ProjectsVC") as! ProjectsVC
            
            Helper.shared.setupMMDrawer(storyboard: "Resources", vc: "ProjectsVC", centerViewController: centerViewController)
        }
        if type == NotiType.series.rawValue {
            
            let centerViewController = UIStoryboard(name: "Legends", bundle: nil).instantiateViewController(withIdentifier: "SeriesVC") as! SeriesVC
            
            Helper.shared.setupMMDrawer(storyboard: "Legends", vc: "SeriesVC", centerViewController: centerViewController)
        }
        if type == NotiType.series_video.rawValue {
            
            let centerViewController = UIStoryboard(name: "Legends", bundle: nil).instantiateViewController(withIdentifier: "SeriesVC") as! SeriesVC
            
            Helper.shared.setupMMDrawer(storyboard: "Legends", vc: "SeriesVC", centerViewController: centerViewController)
        }
        if type == NotiType.service.rawValue {
            
            let centerViewController = UIStoryboard(name: "Activities", bundle: nil).instantiateViewController(withIdentifier: "ServicesVC") as! ServicesVC
            
            Helper.shared.setupMMDrawer(storyboard: "Activities", vc: "ServicesVC", centerViewController: centerViewController)
        }
        if type == NotiType.video.rawValue {
            
            let centerViewController = UIStoryboard(name: "Videos", bundle: nil).instantiateViewController(withIdentifier: "VideoVC") as! VideoVC
            
            Helper.shared.setupMMDrawer(storyboard: "Videos", vc: "VideoVC", centerViewController: centerViewController)
        }
    }
    // MARK: - Webservice

    func addDeviceDetails(token:String) {
        Helper.shared.showHUD()
        
        let params = ["device_type":"ios",
                      "device_token":token] as [String : Any]
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.project_apply, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                //Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Covid19")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

