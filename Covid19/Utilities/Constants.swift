//
//  Constants.swift
//  Hungriji
//
//  Created by 360Technosoft on 04/05/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit



struct URLs {
    static let baseurl = "http://chessmafia.com/php/covid19/api/"
    
    static let get_services = baseurl + "get-services?search="
    static let get_issues = baseurl + "get-issues?search="
    static let get_researches = baseurl + "get-researches?search="
    static let get_projects = baseurl + "get-projects?search="
    static let project_apply = baseurl + "project-apply"
    static let get_founder_details = baseurl + "get-founder-details"
    static let get_vision = baseurl + "get-vision?search="
    static let get_avatars = baseurl + "get-avatars?search="
    static let get_characters = baseurl + "get-characters?search="
    static let get_series = baseurl + "get-series?search="
    static let get_videos = baseurl + "get-videos?search"
    static let get_prodcasts = baseurl + "get-prodcasts?search="
    static let get_video_details = baseurl + "get-video-details?video_id="
    static let get_literatures = baseurl + "get-literatures?search="
    static let get_home = baseurl + "get-home?search="
    static let add_device_details = baseurl + "add-device-details"
    static let get_webview_url = baseurl + "get-webview-url"
}

struct Message {
    
    static let enterEmail = "Please enter Email"
    static let enterName = "Please enter Name"
    static let validEmail = "Please enter valid Email id"
}
