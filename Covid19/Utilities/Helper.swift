//
//  Helper.swift
//  BitesFunds
//
//  Created by Apple on 12/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import SVProgressHUD
import MMDrawerController
import AVKit
import UIKit

class Helper: NSObject {
    
    static let shared = Helper()
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var lang = "en"
    
    var title = "title"
    var descriptions = "description"
    var description_2 = "description_2"
    var question = "question"
    var answer = "answer"
    var goal = "goal"
    var name = "name"
    var post_details = "post_details"
    var details_1 = "details_1"
    var details_2 = "details_2"
    var details_3 = "details_3"
    var title1 = "title1"
    var title2 = "title2"
    var category = "category"
    
    var strComicUrl = ""
    var strProductUrl = ""
    
    func showHUD() {
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(.custom)
        SVProgressHUD.setBackgroundLayerColor(UIColor.black.withAlphaComponent(0.5))
    }
    
    func hideHUD() {
        
        SVProgressHUD.dismiss()
    }
    
    func createVideoThumbnail(url: URL) -> UIImage? {

        let asset = AVAsset(url: url)
        let assetImgGenerate = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.maximumSize = CGSize(width: Helper.shared.screenWidth, height: 100)

        let time = CMTimeMakeWithSeconds(0.0, preferredTimescale: 600)
        do {
            let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let thumbnail = UIImage(cgImage: img)
            return thumbnail
        }
        catch {
          print(error.localizedDescription)
          return nil
        }
    }
    
    func setupMMDrawer(storyboard:String, vc:String, centerViewController:UIViewController) {
        
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let leftViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        
        let leftSideNav = UINavigationController(rootViewController: leftViewController)
        leftSideNav.navigationBar.isHidden = true
        let centerNav = UINavigationController(rootViewController: centerViewController)
        centerNav.navigationBar.isHidden = true
        appDelegate.centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav)
        
        appDelegate.centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView
        appDelegate.centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.tapCenterView
        appDelegate.window?.rootViewController = appDelegate.centerContainer
        appDelegate.centerContainer?.maximumLeftDrawerWidth = 350
//        let navController = appDelegate.window?.rootViewController as? UINavigationController
//        navController?.pushViewController(appDelegate.centerContainer!, animated: true)
    }
    
}
enum NotiType:String {
    
    case article = "article"
    case avatar = "avatar"
    case book = "book"
    case character = "character"
    case comic = "comic"
    case issue = "issue"
    case journal = "journal"
    case prodcast = "prodcast"
    case project = "project"
    case series = "series"
    case series_video = "series-video"
    case service = "service"
    case video = "video"
}
