//
//  PodcastDetailsVC.swift
//  Covid19
//
//  Created by Apple on 05/03/21.
//

import UIKit

class PodcastDetailsVC: UIViewController {

    // MARK: - Variable
    var dictPodcast:PodcastListData?
    var dictComic:ComicListData?
    var dictVideo:VideoListData?
    
    
    var strType = ""
    
    // MARK: - IBOutlet
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        if strType == Type.comic.rawValue {
            lblTitle.text = dictComic?.title
            lblDesc.text = ""
            img.sd_setImage(with: URL(string: dictComic?.banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        }
        if strType == Type.video.rawValue {
            lblTitle.text = dictVideo?.title
            lblDesc.text = dictVideo?.description
            img.image = Helper.shared.createVideoThumbnail(url: URL(string: dictVideo?.video ?? "")!)
        }
        if strType == Type.voice.rawValue {
            lblTitle.text = dictPodcast?.title
            lblDesc.text = dictPodcast?.description
            img.sd_setImage(with: URL(string: dictPodcast?.banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        }
        
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: -

}
enum Type :String{
    case comic
    case video
    case voice
    case journal
    case article
    case book
}
