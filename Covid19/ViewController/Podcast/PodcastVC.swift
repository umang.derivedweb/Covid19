//
//  PodcastVC.swift
//  Covid19
//
//  Created by Apple on 05/03/21.
//

import UIKit

class PodcastVC: UIViewController {

    // MARK: - Variable
    var arrPodcast:[PodcastListData]?
    var arrComic:[ComicListData]?
    var arrVideo:[VideoListData]?
    
    // MARK: - IBoutlet
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var collviewComic: UICollectionView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var collviewVideo: UICollectionView!
    @IBOutlet weak var lblComic: UILabel!
    @IBOutlet weak var lblVoices: UILabel!
    @IBOutlet weak var collviewVoice: UICollectionView!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var lblVideo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getPodcastAPI(search: "")
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        
        lblVideo.text = "Videos".localizableString(lang: Helper.shared.lang)
        lblVoices.text = "Voices".localizableString(lang: Helper.shared.lang)
        lblComic.text = "Comics".localizableString(lang: Helper.shared.lang)
        txtSearch.attributedPlaceholder = NSAttributedString(string:"Search".localizableString(lang: Helper.shared.lang), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    @objc func getHintsFromTextField(textField: UITextField) {
        print(textField.text)
        getPodcastAPI(search: textField.text!)
        
    }
    //MARK: - Webservice
    func getPodcastAPI(search:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetPodcast(url: "\(URLs.get_prodcasts)\(search)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrVideo = response.data?.videos?.data
                self.arrPodcast = response.data?.podcasts?.data
                self.arrComic = response.data?.comocs?.data
                self.collviewComic.reloadData()
                self.collviewVoice.reloadData()
                self.collviewVideo.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: UIButton) {
//        if containerview.isHidden == true {
//            containerview.isHidden = false
//            self.view.bringSubviewToFront(containerview)
//        } else {
//            containerview.isHidden = true
//        }
        
        if sender.tag == 1 {
            heightView.constant = 0
            sender.tag = 2
        } else {
            sender.tag = 1
            heightView.constant = 50
        }
    }
    
    @IBAction func btnSeeAllComics(_ sender: Any) {
    }
    
    @IBAction func btnSeeAllVideo(_ sender: Any) {
        let obj = UIStoryboard(name: "Videos", bundle: nil).instantiateViewController(withIdentifier: "VideoVC") as! VideoVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnSeeAllVoices(_ sender: Any) {
    }
}
extension PodcastVC:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == collviewComic {
            
            return arrComic?.count ?? 0
        }
        if collectionView == collviewVideo {
            
            return arrVideo?.count ?? 0
        }
        if collectionView == collviewVoice {
            
            return arrPodcast?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collviewComic {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComicCell", for: indexPath) as! ComicCell
            cell.viewWithTag(1)?.shadowToView()
            cell.lblTitle.text = arrComic?[indexPath.row].title
            cell.img.sd_setImage(with: URL(string: arrComic?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            return cell
        }
        if collectionView == collviewVideo {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollCell", for: indexPath) as! VideoCollCell
            cell.viewWithTag(1)?.shadowToView()
            cell.lblTitle.text = arrVideo?[indexPath.row].title
            cell.lblDesc.text = arrVideo?[indexPath.row].description
            cell.img.sd_setImage(with: URL(string: arrVideo?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            return cell
        }
        if collectionView == collviewVoice {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollCell", for: indexPath) as! VideoCollCell
            cell.viewWithTag(2)?.shadowToView()
            cell.lblTitle.text = arrPodcast?[indexPath.row].title
            cell.lblDesc.text = arrPodcast?[indexPath.row].description
            cell.img.sd_setImage(with: URL(string: arrPodcast?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            return cell
        }
        
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collviewComic {
            
            return CGSize(width: (collectionView.frame.width/2)-20, height: 210)
        }else {
            
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = UIStoryboard(name: "Podcast", bundle: nil).instantiateViewController(withIdentifier: "PodcastDetailsVC") as! PodcastDetailsVC
        if collectionView == collviewComic {
            obj.strType = Type.comic.rawValue
            obj.dictComic = arrComic?[indexPath.row]
        }
        if collectionView == collviewVideo {
            
            obj.strType = Type.video.rawValue
            obj.dictVideo = arrVideo?[indexPath.row]
        }
        if collectionView == collviewVoice {
            
            obj.strType = Type.voice.rawValue
            obj.dictPodcast = arrPodcast?[indexPath.row]
        }
        navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
extension PodcastVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(self.getHintsFromTextField),
            object: textField)
        self.perform(
            #selector(self.getHintsFromTextField),
            with: textField,
            afterDelay: 1.0)
        return true
    }

}
