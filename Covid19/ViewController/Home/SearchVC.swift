//
//  SearchVC.swift
//  Covid19
//
//  Created by Apple on 02/03/21.
//

import UIKit

class SearchVC: UIViewController {
    
    // MARK: - Variables
    var tableHeight:CGFloat = 0.0
    
    // MARK: - IBOutlet
    //@IBOutlet weak var table: UITableView!
    @IBOutlet weak var table: ContentSizedTableView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var heightTable: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
        print(table.frame.height)
        tableHeight = CGFloat(table.frame.height)
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        if self.table.contentSize.height < tableHeight {
            self.heightTable?.constant = self.table.contentSize.height
        } else {
            self.heightTable?.constant = tableHeight
        }
        
    }
    
    
    // MARK: - Function
    
    func setupUI() {
        
        table.rowHeight = 70
        txtSearch.attributedPlaceholder = NSAttributedString(string:"Search".localizableString(lang: Helper.shared.lang), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    
    @objc func getHintsFromTextField(textField: UITextField) {
        print(textField.text)
        
    }
    
    // MARK: - Webservice
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
extension SearchVC:UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell") as! SearchCell
        cell.lblTitle.text = "Title"
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        self.viewWillLayoutSubviews()
    }
}
extension SearchVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(self.getHintsFromTextField),
            object: textField)
        self.perform(
            #selector(self.getHintsFromTextField),
            with: textField,
            afterDelay: 1.0)
        return true
    }
    
}
final class ContentSizedTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}
