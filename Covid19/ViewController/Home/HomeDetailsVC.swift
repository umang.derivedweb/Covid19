//
//  HomeDetailsVC.swift
//  Covid19
//
//  Created by Apple on 02/03/21.
//

import UIKit

class HomeDetailsVC: UIViewController {

    // MARK: - Variable
    var dictService:ServicesListData?
    var dictResearch:ReserchesListData?
    var dictHome:FeaturedData?
    var strFrom = ""
    
    // MARK: - IBOutlet
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblShareWith: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        if strFrom == "Service" {
            lblTitle.text = dictService?.title
            lblDesc.text = dictService?.description
            img.sd_setImage(with: URL(string: dictService?.banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        }
        if strFrom == "Research" {
            lblTitle.text = dictResearch?.title
            lblDesc.text = dictResearch?.description
            img.sd_setImage(with: URL(string: dictResearch?.banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        }
        if strFrom == "Home" {
            lblTitle.text = dictHome?.title
            lblDesc.text = dictHome?.description
            img.sd_setImage(with: URL(string: dictHome?.banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        }
        
        lblShareWith.text = "Share with".localizableString(lang: Helper.shared.lang)
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSocial(_ sender: UIButton) {
    }
    
    
    
    

}
