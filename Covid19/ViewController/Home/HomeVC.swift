//
//  HomeVC.swift
//  Covid19
//
//  Created by Apple on 02/03/21.
//

import UIKit

class HomeVC: UIViewController {

    // MARK: - Variables
    var dict:HomeResponse?
    
    // MARK: - IBOutlet
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var btnHeader: UIButton!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var txtSearch: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getHomeAPI(search: "")
        getWebviewUrlAPI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        
        table.rowHeight = UITableView.automaticDimension
        table.estimatedRowHeight = 50
        
        let nib = UINib(nibName: "TableSectionHeader", bundle: nil)
        table.register(nib, forHeaderFooterViewReuseIdentifier: "TableSectionHeader")
        
        self.table.register(UINib(nibName: "SearchCell", bundle: nil), forCellReuseIdentifier: "SearchCell")
        txtSearch.attributedPlaceholder = NSAttributedString(string:"Search".localizableString(lang: Helper.shared.lang), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    @objc func getHintsFromTextField(textField: UITextField) {
        print(textField.text)
        getHomeAPI(search: textField.text!)
        
    }
    //MARK: - Webservice
    func getHomeAPI(search:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetHome(url: "\(URLs.get_home)\(search)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.dict = response
                self.table.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    func getWebviewUrlAPI() {
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetWebviewUrl(url: "\(URLs.get_webview_url)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                Helper.shared.strComicUrl = response.data?.comics_url ?? ""
                Helper.shared.strProductUrl = response.data?.product_url ?? ""
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
        }
    }
    // MARK: - IBAction
    
    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: UIButton) {
//        if containerview.isHidden == true {
//            containerview.isHidden = false
//            self.view.bringSubviewToFront(containerview)
//        } else {
//            containerview.isHidden = true
//        }
        
        if sender.tag == 1 {
            heightView.constant = 0
            sender.tag = 2
        } else {
            sender.tag = 1
            heightView.constant = 50
        }
    }

}
extension HomeVC:UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return dict?.popular?.data?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FeaturedCell") as! FeaturedCell
            cell.lblDesc.text = dict?.featured?.description
            cell.img.sd_setImage(with: URL(string: dict?.featured?.banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PopularCell") as! PopularCell
            cell.lblTitle.text = dict?.popular?.data?[indexPath.row].title
            cell.lblDesc.text = dict?.popular?.data?[indexPath.row].description
            cell.img.sd_setImage(with: URL(string: dict?.popular?.data?[indexPath.row].banner_thumb ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let cell = self.table.dequeueReusableHeaderFooterView(withIdentifier: "TableSectionHeader") as! TableSectionHeader
        cell.contentView.backgroundColor = #colorLiteral(red: 0.9531012177, green: 0.9531235099, blue: 0.9531114697, alpha: 1)
        if section == 0 {
            cell.lblTitle.text = "Featured".localizableString(lang: Helper.shared.lang)
        } else {
            cell.lblTitle.text = "Popular".localizableString(lang: Helper.shared.lang)
        }

        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "HomeDetailsVC") as! HomeDetailsVC
        obj.strFrom = "Home"
        if indexPath.section == 0 {
            obj.dictHome = dict?.featured
        } else {
            obj.dictHome = dict?.popular?.data?[indexPath.row]
        }
        navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
extension HomeVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(self.getHintsFromTextField),
            object: textField)
        self.perform(
            #selector(self.getHintsFromTextField),
            with: textField,
            afterDelay: 1.0)
        return true
    }

}
