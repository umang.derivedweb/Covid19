//
//  ProjectDetailsVC.swift
//  Covid19
//
//  Created by Apple on 04/03/21.
//

import UIKit

class ProjectDetailsVC: UIViewController {

    // MARK: - Variable
    var dictProject:ProjectListData?
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnApply: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblGoal: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblProjectGoal: UILabel!
    @IBOutlet weak var imhTimeline: UIImageView!
    @IBOutlet weak var lblProjectTimeline: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        lblDesc.text = dictProject?.description
        lblTitle.text = dictProject?.title
        lblGoal.text = dictProject?.goal
        img.sd_setImage(with: URL(string: dictProject?.banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        imhTimeline.sd_setImage(with: URL(string: dictProject?.timeline ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        
        lblProjectGoal.text = "Project Goal".localizableString(lang: Helper.shared.lang)
        lblProjectTimeline.text = "Project Timeline".localizableString(lang: Helper.shared.lang)
        btnApply.setTitle("Apply".localizableString(lang: Helper.shared.lang), for: .normal)
        
        txtName.attributedPlaceholder = NSAttributedString(string:"Name".localizableString(lang: Helper.shared.lang), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        txtEmail.attributedPlaceholder = NSAttributedString(string:"Email".localizableString(lang: Helper.shared.lang), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnApply(_ sender: Any) {
        
        if txtEmail.text?.isEmpty ?? true {
            Toast.show(message: Message.enterEmail, controller: self)
            return
        } else {
            if !(txtEmail.text)!.isValidEmail() {
                Toast.show(message: Message.validEmail, controller: self)
                return
            }
        }
        if txtName.text?.isEmpty ?? true {
            Toast.show(message: Message.enterName, controller: self)
            return
        }
        applyProjectAPI()
    }
    
    // MARK: - Webservice

    func applyProjectAPI() {
        Helper.shared.showHUD()
        
        let params = ["project_id":dictProject?.project_id!,
                      "name":txtName.text!,
                      "email":txtEmail.text!] as [String : Any]
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallCommon(url: URLs.project_apply, parameters: params, headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.txtName.text = ""
                self.txtEmail.text = ""
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
}
