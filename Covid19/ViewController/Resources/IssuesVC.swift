//
//  IssuesVC.swift
//  Covid19
//
//  Created by Apple on 04/03/21.
//

import UIKit
import SDWebImage

class IssuesVC: UIViewController {

    //MARK: Variables

    var arrImages:[String] = []

    var arrIssuesList:[IssuesListData]?
    var arrQue:[FaqData]?

    var selectedIndex = -1

    // i.e initially no row is selected

    var isExpanded = false
    
    //MARK: IBOutlets
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblTitle1: UILabel!
    @IBOutlet weak var lblIssues: UILabel!
    @IBOutlet weak var collectionview: UICollectionView!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getIssuesAPI(search: "")
    }
    
    //MARK: Function
    
    func setupUI() {
        navigationController?.navigationBar.isHidden = true
        table.estimatedRowHeight = 56
        table.rowHeight = UITableView.automaticDimension
        table.tableFooterView = UIView()
        
        txtSearch.attributedPlaceholder = NSAttributedString(string:"Search".localizableString(lang: Helper.shared.lang), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        lblIssues.text = "Issues".localizableString(lang: Helper.shared.lang)
        lblTitle1.text = "Title 1".localizableString(lang: Helper.shared.lang)
    }
    @objc func getHintsFromTextField(textField: UITextField) {
        print(textField.text)
        getIssuesAPI(search: textField.text!)
        
    }
    //MARK: - Webservice
    func getIssuesAPI(search:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetIssues(url: "\(URLs.get_issues)\(search)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrIssuesList = response.issues?.data
                
                self.arrQue = self.arrIssuesList?[0].faq
                self.arrImages.removeAll()
                for _ in 0..<(self.arrQue?.count ?? 0) {
                    
                    self.arrImages.append("down")
                }
                self.table.reloadData()
                self.collectionview.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }

    //MARK: IBAction
    
    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: UIButton) {
//        if containerview.isHidden == true {
//            containerview.isHidden = false
//            self.view.bringSubviewToFront(containerview)
//        } else {
//            containerview.isHidden = true
//        }
        
        if sender.tag == 1 {
            heightView.constant = 0
            sender.tag = 2
        } else {
            sender.tag = 1
            heightView.constant = 50
        }
    }

}

extension IssuesVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrQue?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TitleCell") as! TitleCell
        
        cell.lblQue.text = arrQue?[indexPath.row].question
        cell.lblAns.text = arrQue?[indexPath.row].answer
        cell.imgArrow.image = UIImage(named: arrImages[indexPath.row])
        
//        if cell.imgArrow.image == UIImage(named: "down") {
//
//            cell.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        } else {
//            cell.viewWithTag(1)?.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.968627451, alpha: 1)
//        }
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (self.selectedIndex == indexPath.row && isExpanded == true) {
            
            return UITableView.automaticDimension
        } else {
            
            return 65
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in 0..<arrImages.count {
            
            arrImages.remove(at: i)
            arrImages.insert("down", at: i)
        }
        //tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedIndex == indexPath.row {
            
            if self.isExpanded == false {
                
                self.isExpanded = true
                
                self.selectedIndex = indexPath.row
                arrImages.remove(at: indexPath.row)
                arrImages.insert("up", at: indexPath.row)
            } else {
                
                self.isExpanded = false
                
                self.selectedIndex = indexPath.row
                arrImages.remove(at: indexPath.row)
                arrImages.insert("down", at: indexPath.row)
            }
        } else {
            
            self.isExpanded = true
            
            self.selectedIndex = indexPath.row
            arrImages.remove(at: indexPath.row)
            arrImages.insert("up", at: indexPath.row)
        }
        
        //tableView.reloadRows(at: [indexPath], with: .automatic)
        tableView.reloadData()
    }
    
}
extension IssuesVC:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrIssuesList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IssuesCell", for: indexPath) as! IssuesCell
        cell.viewWithTag(1)?.shadowToView()
        cell.img.sd_setImage(with: URL(string: arrIssuesList?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        cell.lblDesc.text = arrIssuesList?[indexPath.row].description
        cell.lblTitle.text = arrIssuesList?[indexPath.row].title
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.collectionview.frame.width, height: 300)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
//        navigationController?.pushViewController(obj, animated: true)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in collectionview.visibleCells {
            let indexPath = collectionview.indexPath(for: cell)
            print(indexPath)
            
            arrQue = arrIssuesList?[indexPath!.row].faq
            self.arrImages.removeAll()
            for _ in 0..<(arrQue?.count ?? 0) {
                
                arrImages.append("down")
            }
            table.reloadData()
        }
    }
    
}
extension IssuesVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(self.getHintsFromTextField),
            object: textField)
        self.perform(
            #selector(self.getHintsFromTextField),
            with: textField,
            afterDelay: 1.0)
        return true
    }

}
