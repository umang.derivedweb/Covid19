//
//  ResearchVC.swift
//  Covid19
//
//  Created by Apple on 04/03/21.
//

import UIKit

class ResearchVC: UIViewController {

    // MARK: - Variable
    var arrResearchesList:[ReserchesListData]?
    
    // MARK: - IBoutlet
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var lblResearch: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getResearchsAPI(search: "")
    }
    @objc func getHintsFromTextField(textField: UITextField) {
        print(textField.text)
        getResearchsAPI(search: textField.text!)
        
    }
    //MARK: - Webservice
    func getResearchsAPI(search:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetResearches(url: "\(URLs.get_researches)\(search)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrResearchesList = response.data?.data
                self.table.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        table.tableFooterView = UIView()
        table.estimatedRowHeight = 140
        table.rowHeight = UITableView.automaticDimension
        
        lblResearch.text = "Research".localizableString(lang: Helper.shared.lang)
        txtSearch.attributedPlaceholder = NSAttributedString(string:"Search".localizableString(lang: Helper.shared.lang), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    
    // MARK: - IBAction

    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: UIButton) {
//        if containerview.isHidden == true {
//            containerview.isHidden = false
//            self.view.bringSubviewToFront(containerview)
//        } else {
//            containerview.isHidden = true
//        }
        
        if sender.tag == 1 {
            heightView.constant = 0
            sender.tag = 2
        } else {
            sender.tag = 1
            heightView.constant = 50
        }
    }
}
extension ResearchVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrResearchesList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResearchCell") as! ResearchCell
        cell.viewWithTag(1)?.shadowToView()
        //cell.lblTitle.text = arrResearchesList?[indexPath.row].title
        cell.lblDesc.text = arrResearchesList?[indexPath.row].description
        cell.img.sd_setImage(with: URL(string: arrResearchesList?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        
        cell.btnLearnMore.setTitle(NSLocalizedString("Learn more".localizableString(lang: Helper.shared.lang), comment: ""),for: .normal)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeDetailsVC") as! HomeDetailsVC
        obj.dictResearch = arrResearchesList?[indexPath.row]
        obj.strFrom = "Research"
        navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
extension ResearchVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(self.getHintsFromTextField),
            object: textField)
        self.perform(
            #selector(self.getHintsFromTextField),
            with: textField,
            afterDelay: 1.0)
        return true
    }

}
