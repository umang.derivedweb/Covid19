//
//  LanguageVC.swift
//  Covid19
//
//  Created by Apple on 05/03/21.
//

import UIKit

class LanguageVC: UIViewController {

    // MARK: - Variable
    
    // MARK: - IBOutlet
    @IBOutlet weak var btnFrench: UIButton!
    @IBOutlet weak var btnEnglist: UIButton!
    @IBOutlet weak var btnKinya: UIButton!
    @IBOutlet weak var btnSwahili: UIButton!
    @IBOutlet weak var lblLanguage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    
    func setupUI() {
        if UserDefaults.standard.value(forKey: "Language") as? String != nil && UserDefaults.standard.value(forKey: "Language") as? String != "" {
            
            Helper.shared.lang = UserDefaults.standard.value(forKey: "Language") as! String
        }
        lblLanguage.text = "Language".localizableString(lang: Helper.shared.lang)
        
        if Helper.shared.lang == "en" {
            
            btnEnglist.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
            btnFrench.setImage(nil, for: .normal)
            btnKinya.setImage(nil, for: .normal)
            btnSwahili.setImage(nil, for: .normal)
        }
        if Helper.shared.lang == "fr" {
            
            btnEnglist.setImage(nil, for: .normal)
            btnFrench.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
            btnKinya.setImage(nil, for: .normal)
            btnSwahili.setImage(nil, for: .normal)
        }
        if Helper.shared.lang == "rw-RW" {
            
            btnEnglist.setImage(nil, for: .normal)
            btnFrench.setImage(nil, for: .normal)
            btnKinya.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
            btnSwahili.setImage(nil, for: .normal)
        }
        if Helper.shared.lang == "sw" {
            
            btnEnglist.setImage(nil, for: .normal)
            btnFrench.setImage(nil, for: .normal)
            btnKinya.setImage(nil, for: .normal)
            btnSwahili.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func btnLanguageChange(_ sender: UIButton) {
        
        if sender.tag == 1 {
            UserDefaults.standard.setValue("en", forKey: "Language")
            UserDefaults.standard.synchronize()
            
            btnEnglist.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
            btnFrench.setImage(nil, for: .normal)
            btnKinya.setImage(nil, for: .normal)
            btnSwahili.setImage(nil, for: .normal)
        }
        if sender.tag == 2 {
            UserDefaults.standard.setValue("fr", forKey: "Language")
            UserDefaults.standard.synchronize()
            btnEnglist.setImage(nil, for: .normal)
            btnFrench.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
            btnKinya.setImage(nil, for: .normal)
            btnSwahili.setImage(nil, for: .normal)
        }
        if sender.tag == 3 {
            UserDefaults.standard.setValue("rw-RW", forKey: "Language")
            UserDefaults.standard.synchronize()
            btnEnglist.setImage(nil, for: .normal)
            btnFrench.setImage(nil, for: .normal)
            btnKinya.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
            btnSwahili.setImage(nil, for: .normal)
        }
        if sender.tag == 4 {
            UserDefaults.standard.setValue("sw", forKey: "Language")
            UserDefaults.standard.synchronize()
            btnEnglist.setImage(nil, for: .normal)
            btnFrench.setImage(nil, for: .normal)
            btnKinya.setImage(nil, for: .normal)
            btnSwahili.setImage(#imageLiteral(resourceName: "tick"), for: .normal)
        }
        viewDidLoad()
    }
    
    // MARK: -

}
