//
//  SettingVC.swift
//  Covid19
//
//  Created by Apple on 05/03/21.
//

import UIKit

class SettingVC: UIViewController, UNUserNotificationCenterDelegate {

    // MARK: - Variable
    
    // MARK: - IBoutlet
    @IBOutlet weak var lblLang: UILabel!
    @IBOutlet weak var btnNoti: UIButton!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var lblSelectedLang: UILabel!
    @IBOutlet weak var lblNoti: UILabel!
    @IBOutlet weak var lblSettings: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if Helper.shared.lang == "en" {
            
            lblSelectedLang.text = "English"
        }
        if Helper.shared.lang == "fr" {
            
            lblSelectedLang.text = "French"
        }
        if Helper.shared.lang == "rw-RW" {
            
            lblSelectedLang.text = "Kinyarwanda"
        }
        if Helper.shared.lang == "sw" {
            
            lblSelectedLang.text = "Swahili"
        }
        
        lblSettings.text = "Setting".localizableString(lang: Helper.shared.lang)
        lblNoti.text = "Notification".localizableString(lang: Helper.shared.lang)
        lblLang.text = "Language".localizableString(lang: Helper.shared.lang)
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        
        if UserDefaults.standard.value(forKey: "isNotification") == nil {
            
            UserDefaults.standard.setValue(true, forKey: "isNotification")
            UserDefaults.standard.synchronize()
        }
    
        
        if UserDefaults.standard.value(forKey: "isNotification") as! Bool == true {
            
            btnNoti.setImage(#imageLiteral(resourceName: "on"), for: .normal)
            btnNoti.tag = 1
        } else {
            
            btnNoti.setImage(#imageLiteral(resourceName: "off"), for: .normal)
            btnNoti.tag = 0
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnNotification(_ sender: UIButton) {
        
        if sender.tag == 1 {
            UIApplication.shared.unregisterForRemoteNotifications()
            sender.tag = 0
            sender.setImage(#imageLiteral(resourceName: "off"), for: .normal)
            UserDefaults.standard.setValue(false, forKey: "isNotification")
            UserDefaults.standard.synchronize()
        } else {
            
            let center  = UNUserNotificationCenter.current()
               center.delegate = self
               center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                     if error == nil{
                        DispatchQueue.main.async(execute: {
                           UIApplication.shared.registerForRemoteNotifications()
                        })
                     }
               }

            sender.tag = 1
            sender.setImage(#imageLiteral(resourceName: "on"), for: .normal)
            UserDefaults.standard.setValue(true, forKey: "isNotification")
            UserDefaults.standard.synchronize()
        }
    }
    
    @IBAction func btnLang(_ sender: Any) {
        
        let obj = UIStoryboard(name: "Settiings", bundle: nil).instantiateViewController(withIdentifier: "LanguageVC") as! LanguageVC
        navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: Any) {
        if containerview.isHidden == true {
            containerview.isHidden = false
            self.view.bringSubviewToFront(containerview)
        } else{
            containerview.isHidden = true
        }
    }
   
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // ...register device token with our Time Entry API server via REST
    }


    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        //print("DidFaildRegistration : Device token for push notifications: FAIL -- ")
        //print(error.localizedDescription)
    }

}
