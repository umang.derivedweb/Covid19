//
//  VideoVC.swift
//  Covid19
//
//  Created by Apple on 05/03/21.
//

import UIKit

class VideoVC: UIViewController, playTappedVideo {
    
    

    // MARK: - Variable
    var arrVideo:[VideoListData]?
    
    // MARK: - IBoutlet
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getVideosAPI(search: "")
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        table.tableFooterView = UIView()
        table.estimatedRowHeight = 140
        table.rowHeight = UITableView.automaticDimension
        txtSearch.attributedPlaceholder = NSAttributedString(string:"Search".localizableString(lang: Helper.shared.lang), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    func sendVideoId(id: Int) {
        
        let obj = UIStoryboard(name: "Videos", bundle: nil).instantiateViewController(withIdentifier: "VideoDetailsVC") as! VideoDetailsVC
        obj.videoId = id
        obj.delegate = self
        navigationController?.pushViewController(obj, animated: false)
    }
    @objc func getHintsFromTextField(textField: UITextField) {
        print(textField.text)
        getVideosAPI(search: textField.text!)
        
    }
    //MARK: - Webservice
    func getVideosAPI(search: String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetVideo(url: "\(URLs.get_videos)\(search)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrVideo = response.data?.data
                self.table.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: UIButton) {
//        if containerview.isHidden == true {
//            containerview.isHidden = false
//            self.view.bringSubviewToFront(containerview)
//        } else {
//            containerview.isHidden = true
//        }
        
        if sender.tag == 1 {
            heightView.constant = 0
            sender.tag = 2
        } else {
            sender.tag = 1
            heightView.constant = 50
        }
    }

}
extension VideoVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrVideo?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell") as! VideoCell
        cell.viewWithTag(1)?.shadowToView()
        cell.lblTitle.text = arrVideo?[indexPath.row].title
        cell.lblDesc.text = arrVideo?[indexPath.row].description
        cell.img.sd_setImage(with: URL(string: arrVideo?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = UIStoryboard(name: "Videos", bundle: nil).instantiateViewController(withIdentifier: "VideoDetailsVC") as! VideoDetailsVC
        obj.videoId = arrVideo?[indexPath.row].video_id
        obj.delegate = self
        navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
extension VideoVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(self.getHintsFromTextField),
            object: textField)
        self.perform(
            #selector(self.getHintsFromTextField),
            with: textField,
            afterDelay: 1.0)
        return true
    }

}
