//
//  VideoDetailsVC.swift
//  Covid19
//
//  Created by Apple on 05/03/21.
//

import UIKit
import AVKit

protocol playTappedVideo {
    func sendVideoId(id:Int)
}
class VideoDetailsVC: UIViewController {

    // MARK: - Variable
    var videoId:Int!
    var arrVideo:[VideoListData]?
    var delegate:playTappedVideo?
    
    // MARK: - IBoutlet
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var viewVideo: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblRelatedVideo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getVideoDetailsAPI()
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        table.tableFooterView = UIView()
        table.rowHeight = 110
        
        lblRelatedVideo.text = "Related Videos".localizableString(lang: Helper.shared.lang)
    }
    //MARK: - Webservice
    func getVideoDetailsAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetVideoDetails(url: "\(URLs.get_video_details)\(videoId!)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrVideo = response.related_videos
                self.table.reloadData()
                
                self.lblTitle.text = response.data?.title
                self.lblDesc.text = response.data?.description
                self.lblDate.text = response.data?.date
                
                guard let url = URL(string: response.data?.video ?? "") else { return }
                let player = AVPlayer(url: url)
                player.rate = 1 //auto play
                let playerFrame = CGRect(x: 0, y: 0, width: self.viewVideo.frame.width, height: self.viewVideo.frame.height)
                let playerViewController = AVPlayerViewController()
                playerViewController.videoGravity = .resizeAspectFill
                playerViewController.player = player
                playerViewController.view.frame = playerFrame
                
                self.addChild(playerViewController)
                self.viewVideo.addSubview(playerViewController.view)
                playerViewController.didMove(toParent: self)
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}
extension VideoDetailsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrVideo?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RelatedVideoCell") as! RelatedVideoCell
        cell.lblTitle.text = arrVideo?[indexPath.row].title
        cell.lblSubTitle.text = arrVideo?[indexPath.row].category
        
        cell.img.sd_setImage(with: URL(string: arrVideo?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        navigationController?.popViewController(animated: false)
        delegate?.sendVideoId(id: arrVideo?[indexPath.row].video_id ?? 0)
    }
    
    
}
