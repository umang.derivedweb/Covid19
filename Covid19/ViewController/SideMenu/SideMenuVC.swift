//
//  SideMenuVC.swift
//  Covid19
//
//  Created by Apple on 02/03/21.
//

import UIKit
import MMDrawerController

class SideMenuVC: UIViewController {
    
    //MARK :- Variables
    var sections = [ItemList]()
    
    var items: [ItemList] = [
        ItemList(name: "Activities".localizableString(lang: Helper.shared.lang), items: ["Our Work".localizableString(lang: Helper.shared.lang), "Services".localizableString(lang: Helper.shared.lang), "Products".localizableString(lang: Helper.shared.lang)]),
        ItemList(name: "Resources".localizableString(lang: Helper.shared.lang), items: ["Issues".localizableString(lang: Helper.shared.lang), "Research".localizableString(lang: Helper.shared.lang),"Projects".localizableString(lang: Helper.shared.lang)]),
        ItemList(name: "Profiles".localizableString(lang: Helper.shared.lang), items: ["Story".localizableString(lang: Helper.shared.lang), "Vision".localizableString(lang: Helper.shared.lang),"Purpose".localizableString(lang: Helper.shared.lang)]),
        ItemList(name: "Legends".localizableString(lang: Helper.shared.lang), items: ["Avatar".localizableString(lang: Helper.shared.lang), "Character".localizableString(lang: Helper.shared.lang),"Series".localizableString(lang: Helper.shared.lang)]),
        ItemList(name: "Videos".localizableString(lang: Helper.shared.lang), items: []),
        ItemList(name: "Prodcast".localizableString(lang: Helper.shared.lang), items: []),
        ItemList(name: "Comic".localizableString(lang: Helper.shared.lang), items: []),
        ItemList(name: "Literature".localizableString(lang: Helper.shared.lang), items: []),
        ItemList(name: "Setting".localizableString(lang: Helper.shared.lang), items: [])
    ]
    
    //MARK :- IBOutlet
    
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        table.reloadData()
    }
    //MARK :- Function
    func setupUI() {
        
        table.tableFooterView = UIView()
        
    }
    
    //MARK :- Structure
    struct ItemList {
        var name: String
        var items: [String]
        var minus: Bool
        
        init(name: String, items: [String], minus: Bool = false) {
            self.name = name
            self.items = items
            self.minus = minus
        }
    }
    
    
}
extension SideMenuVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        /////////
        if section == 1 || section == 3 || section == 5 || section == 7 {
            return 0
        }
        ////////
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerHeading = UILabel(frame: CGRect(x: 5, y: 10, width: self.view.frame.width, height: 40))
        headerHeading.font = UIFont(name:"OpenSans-Semibold",size:17)
        let bottomLine = UILabel(frame: CGRect(x: 10, y: 49, width: self.view.frame.width-20, height: 1))
        bottomLine.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        let imageView = UIImageView(frame: CGRect(x: self.view.frame.width - 30, y: 20, width: 20, height: 20))
        if items[section].minus{
            imageView.image = UIImage(named: "minus")
        }else{
            imageView.image = UIImage(named: "plus")
        }
        if section == 4 || section == 5 || section == 6 || section == 7 || section == 8 {
            imageView.isHidden = true
        }
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        let tapGuesture = UITapGestureRecognizer(target: self, action: #selector(headerViewTapped))
        tapGuesture.numberOfTapsRequired = 1
        headerView.addGestureRecognizer(tapGuesture)
        headerView.backgroundColor = #colorLiteral(red: 0.1137254902, green: 0.1137254902, blue: 0.1137254902, alpha: 1)
        headerView.tag = section
        headerHeading.text = items[section].name
        headerHeading.textColor = .white
        headerView.addSubview(headerHeading)
        headerView.addSubview(bottomLine)
        headerView.addSubview(imageView)
        
        
        return headerView
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let itms = items[section]
        return !itms.minus ? 0 : itms.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        cell.lblTitle.text = items[indexPath.section].items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            
            if indexPath.row == 0 {
                
                let obj = UIStoryboard(name: "Activities", bundle: nil).instantiateViewController(withIdentifier: "OurWorkVC") as! OurWorkVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 1 {
                
                let obj = UIStoryboard(name: "Activities", bundle: nil).instantiateViewController(withIdentifier: "ServicesVC") as! ServicesVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 2 {
                
                let obj = UIStoryboard(name: "Activities", bundle: nil).instantiateViewController(withIdentifier: "ProductVC") as! ProductVC
                obj.isFrom = "Product"
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
        }
        if indexPath.section == 1 {
            
            if indexPath.row == 0 {
                
                let obj = UIStoryboard(name: "Resources", bundle: nil).instantiateViewController(withIdentifier: "IssuesVC") as! IssuesVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 1 {
                
                let obj = UIStoryboard(name: "Resources", bundle: nil).instantiateViewController(withIdentifier: "ResearchVC") as! ResearchVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 2 {
                
                let obj = UIStoryboard(name: "Resources", bundle: nil).instantiateViewController(withIdentifier: "ProjectsVC") as! ProjectsVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
        }
        if indexPath.section == 2 {
            
            if indexPath.row == 0 {
                
                let obj = UIStoryboard(name: "Profiles", bundle: nil).instantiateViewController(withIdentifier: "StoryVC") as! StoryVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 1 {
                
                let obj = UIStoryboard(name: "Profiles", bundle: nil).instantiateViewController(withIdentifier: "VisionVC") as! VisionVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 2 {
                
                let obj = UIStoryboard(name: "Profiles", bundle: nil).instantiateViewController(withIdentifier: "PurposeVC") as! PurposeVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
        }
        if indexPath.section == 3 {
            
            if indexPath.row == 0 {
                
                let obj = UIStoryboard(name: "Legends", bundle: nil).instantiateViewController(withIdentifier: "AvatarVC") as! AvatarVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 1 {
                
                let obj = UIStoryboard(name: "Legends", bundle: nil).instantiateViewController(withIdentifier: "CharacterVC") as! CharacterVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            if indexPath.row == 2 {
                
                let obj = UIStoryboard(name: "Legends", bundle: nil).instantiateViewController(withIdentifier: "SeriesVC") as! SeriesVC
                let navController = UINavigationController(rootViewController: obj)
                navController.navigationBar.isHidden = true
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.centerContainer!.centerViewController = navController
                appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
        }
    }
    @objc func headerViewTapped(tapped:UITapGestureRecognizer){
        print(tapped.view?.tag)
        
        if tapped.view?.tag == 4 {
            let obj = UIStoryboard(name: "Videos", bundle: nil).instantiateViewController(withIdentifier: "VideoVC") as! VideoVC
            let navController = UINavigationController(rootViewController: obj)
            navController.navigationBar.isHidden = true
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = navController
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            return
        }
        if tapped.view?.tag == 5 {
            let obj = UIStoryboard(name: "Podcast", bundle: nil).instantiateViewController(withIdentifier: "PodcastVC") as! PodcastVC
            let navController = UINavigationController(rootViewController: obj)
            navController.navigationBar.isHidden = true
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = navController
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            return
        }
        if tapped.view?.tag == 6 {
            let obj = UIStoryboard(name: "Activities", bundle: nil).instantiateViewController(withIdentifier: "ProductVC") as! ProductVC
            obj.isFrom = "Comic"
            let navController = UINavigationController(rootViewController: obj)
            navController.navigationBar.isHidden = true
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = navController
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        if tapped.view?.tag == 7 {
            let obj = UIStoryboard(name: "Literature", bundle: nil).instantiateViewController(withIdentifier: "LiteratureVC") as! LiteratureVC
            let navController = UINavigationController(rootViewController: obj)
            navController.navigationBar.isHidden = true
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = navController
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            return
        }
        if tapped.view?.tag == 8 {
            let obj = UIStoryboard(name: "Settiings", bundle: nil).instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            let navController = UINavigationController(rootViewController: obj)
            navController.navigationBar.isHidden = true
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.centerContainer!.centerViewController = navController
            appDelegate.centerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            return
        }
        if items[tapped.view!.tag].minus == true{
            items[tapped.view!.tag].minus = false
        }else{
            items[tapped.view!.tag].minus = true
        }
        if let imView = tapped.view?.subviews[1] as? UIImageView{
            if imView.isKind(of: UIImageView.self){
                if items[tapped.view!.tag].minus{
                    imView.image = UIImage(named: "minus")
                }else{
                    imView.image = UIImage(named: "plus")
                }
            }
        }
        table.reloadData()
    }
   
    
}
