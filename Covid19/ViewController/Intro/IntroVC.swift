//
//  IntroVC.swift
//  Covid19
//
//  Created by Apple on 02/03/21.
//

import UIKit
import MMDrawerController

class IntroVC: UIViewController {

    @IBOutlet weak var lblResources: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnStart: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        lblResources.text = "Helpful Resources".localizableString(lang: Helper.shared.lang)
        lblDesc.text = "We believe in the power of individual philosophy, solution-oriented initiative(s) and the mastery of one’s given challenges in life. Therefore, transferring of knowledge from generation to generation, from professionals to students, and from masters to initiates is an important part of our work; and a key fact in our programs.".localizableString(lang: Helper.shared.lang)
        
        btnStart.setTitle("Let's start".localizableString(lang: Helper.shared.lang), for: .normal)
    }
    
    @IBAction func btnStart(_ sender: Any) {
        
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let centerViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        let leftViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        
        let leftSideNav = UINavigationController(rootViewController: leftViewController)
        leftSideNav.navigationBar.isHidden = true
        let centerNav = UINavigationController(rootViewController: centerViewController)
        centerNav.navigationBar.isHidden = true
        appDelegate.centerContainer = MMDrawerController(center: centerNav, leftDrawerViewController: leftSideNav)
        
        appDelegate.centerContainer!.openDrawerGestureModeMask = MMOpenDrawerGestureMode.panningCenterView
        appDelegate.centerContainer!.closeDrawerGestureModeMask = MMCloseDrawerGestureMode.tapCenterView
        //appDelegate.window?.rootViewController = appDelegate.centerContainer
        appDelegate.centerContainer?.maximumLeftDrawerWidth = 350
        self.navigationController?.pushViewController(appDelegate.centerContainer!, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
