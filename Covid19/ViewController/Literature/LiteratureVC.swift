//
//  LiteratureVC.swift
//  Covid19
//
//  Created by Apple on 16/03/21.
//

import UIKit

class LiteratureVC: UIViewController {

    // MARK: - Variable
    var arrJournals:[JournalListData]?
    var arrArticle:[ArtclesListData]?
    var arrBook:[BooksListData]?
    
    // MARK: - IBoutlet
    @IBOutlet weak var lblArtcles: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblBooks: UILabel!
    @IBOutlet weak var lblJournals: UILabel!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var collviewJournal: UICollectionView!
    @IBOutlet weak var collviewArticle: UICollectionView!
    @IBOutlet weak var collviewBooks: UICollectionView!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getLitaratureAPI(search: "")
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        
        lblBooks.text = "Books".localizableString(lang: Helper.shared.lang)
        lblArtcles.text = "Articles".localizableString(lang: Helper.shared.lang)
        lblJournals.text = "Journals".localizableString(lang: Helper.shared.lang)
        txtSearch.attributedPlaceholder = NSAttributedString(string:"Search".localizableString(lang: Helper.shared.lang), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
    }
    @objc func getHintsFromTextField(textField: UITextField) {
        print(textField.text)
        getLitaratureAPI(search: textField.text!)
        
    }
    //MARK: - Webservice
    func getLitaratureAPI(search:String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetLiterature(url: "\(URLs.get_literatures)\(search)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrJournals = response.journals?.data
                self.arrArticle = response.articals?.data
                self.arrBook = response.books?.data
                self.collviewJournal.reloadData()
                self.collviewBooks.reloadData()
                self.collviewArticle.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: UIButton) {
//        if containerview.isHidden == true {
//            containerview.isHidden = false
//            self.view.bringSubviewToFront(containerview)
//        } else {
//            containerview.isHidden = true
//        }
        
        if sender.tag == 1 {
            heightView.constant = 0
            sender.tag = 2
        } else {
            sender.tag = 1
            heightView.constant = 50
        }
    }
    
    @IBAction func btnSeeAllJournals(_ sender: Any) {
    }
    
    @IBAction func btnSeeAllArticle(_ sender: Any) {
    }
    
    @IBAction func btnSeeAllBooks(_ sender: Any) {
    }
}
extension LiteratureVC:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if collectionView == collviewJournal {
            
            return arrJournals?.count ?? 0
        }
        if collectionView == collviewArticle {
            
            return arrArticle?.count ?? 0
        }
        if collectionView == collviewBooks {
            
            return arrBook?.count ?? 0
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collviewJournal {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComicCell", for: indexPath) as! ComicCell
            cell.viewWithTag(2)?.shadowToView()
            cell.lblTitle.text = arrJournals?[indexPath.row].title
            cell.img.sd_setImage(with: URL(string: arrJournals?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            return cell
        }
        if collectionView == collviewArticle {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCollCell", for: indexPath) as! VideoCollCell
            cell.viewWithTag(3)?.shadowToView()
            cell.lblTitle.text = arrArticle?[indexPath.row].title
            cell.lblDesc.text = arrArticle?[indexPath.row].description
            cell.img.sd_setImage(with: URL(string: arrArticle?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            return cell
        }
        if collectionView == collviewBooks {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ComicCell", for: indexPath) as! ComicCell
            cell.viewWithTag(3)?.shadowToView()
            cell.lblTitle.text = arrBook?[indexPath.row].title
            cell.img.sd_setImage(with: URL(string: arrBook?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collviewJournal || collectionView == collviewBooks {
            
            return CGSize(width: (collectionView.frame.width/2)-20, height: 210)
        }else {
            
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = UIStoryboard(name: "Literature", bundle: nil).instantiateViewController(withIdentifier: "LiteratureDetailsVC") as! LiteratureDetailsVC
        if collectionView == collviewJournal {
            obj.strType = Type.journal.rawValue
            obj.dictJournals = arrJournals?[indexPath.row]
        }
        if collectionView == collviewArticle {
            
            obj.strType = Type.article.rawValue
            obj.dictArticle = arrArticle?[indexPath.row]
        }
        if collectionView == collviewBooks {
            
            obj.strType = Type.book.rawValue
            obj.dictBook = arrBook?[indexPath.row]
        }
        navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
extension LiteratureVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(self.getHintsFromTextField),
            object: textField)
        self.perform(
            #selector(self.getHintsFromTextField),
            with: textField,
            afterDelay: 1.0)
        return true
    }

}
