//
//  LiteratureDetailsVC.swift
//  Covid19
//
//  Created by Apple on 28/04/21.
//

import UIKit

class LiteratureDetailsVC: UIViewController {

    // MARK: - Variable
    var dictJournals:JournalListData?
    var dictArticle:ArtclesListData?
    var dictBook:BooksListData?
    
    var strType = ""
    
    
    // MARK: - IBOutlet
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc1: UILabel!
    @IBOutlet weak var lblDesc2: UILabel!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    func setupUI() {
        
        if strType == Type.journal.rawValue {
            lblTitle.text = dictJournals?.title
            lblDesc1.text = dictJournals?.description
            img1.sd_setImage(with: URL(string: dictJournals?.banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            lblDesc2.text = dictJournals?.description_2
            img2.sd_setImage(with: URL(string: dictJournals?.image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        }
        if strType == Type.article.rawValue {
            lblTitle.text = dictArticle?.title
            lblDesc1.text = dictArticle?.description
            img1.sd_setImage(with: URL(string: dictArticle?.banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            lblDesc2.text = dictArticle?.description_2
            img2.sd_setImage(with: URL(string: dictArticle?.image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        }
        if strType == Type.book.rawValue {
            lblTitle.text = dictBook?.title
            lblDesc1.text = dictBook?.description
            img1.sd_setImage(with: URL(string: dictBook?.banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
            lblDesc2.text = dictBook?.description_2
            img2.sd_setImage(with: URL(string: dictBook?.image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        }
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

}
