//
//  VisionVC.swift
//  Covid19
//
//  Created by Apple on 04/03/21.
//

import UIKit

class VisionVC: UIViewController {

    // MARK: - Variable
    var arrVision:[String] = []
    
    // MARK: - IBoutlet
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblVision: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getVisionAPI()
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        table.tableFooterView = UIView()
        table.estimatedRowHeight = 140
        table.rowHeight = UITableView.automaticDimension
        
        lblVision.text = "Vision".localizableString(lang: Helper.shared.lang)
    }
    //MARK: - Webservice
    func getVisionAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetVision(url: "\(URLs.get_vision)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.img.sd_setImage(with: URL(string: response.data?.banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                self.arrVision.append(response.data?.description ?? "")
                self.table.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: Any) {
        if containerview.isHidden == true {
            containerview.isHidden = false
            self.view.bringSubviewToFront(containerview)
        } else{
            containerview.isHidden = true
        }
    }

}
extension VisionVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrVision.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
        cell.viewWithTag(1)?.shadowToView()
        cell.lblDesc.text = arrVision[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeDetailsVC") as! HomeDetailsVC
//        navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
