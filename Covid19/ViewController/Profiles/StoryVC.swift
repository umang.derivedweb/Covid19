//
//  StoryVC.swift
//  Covid19
//
//  Created by Apple on 04/03/21.
//

import UIKit

class StoryVC: UIViewController {

    // MARK: - Variable
    var arrDetails:[String] = []
    
    // MARK: - IBoutlet
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblOccupation: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getStoryAPI()
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        table.tableFooterView = UIView()
        table.estimatedRowHeight = 140
        table.rowHeight = UITableView.automaticDimension
    }
    
    //MARK: - Webservice
    func getStoryAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetStory(url: "\(URLs.get_founder_details)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrDetails.append(response.data?.details_1 ?? "")
                self.arrDetails.append(response.data?.details_2 ?? "")
                self.arrDetails.append(response.data?.details_3 ?? "")
                self.img.sd_setImage(with: URL(string: response.data?.profile_pic ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
                self.lblName.text = response.data?.name
                self.lblOccupation.text = response.data?.post_details
                self.table.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: Any) {
        if containerview.isHidden == true {
            containerview.isHidden = false
            self.view.bringSubviewToFront(containerview)
        } else{
            containerview.isHidden = true
        }
    }

}
extension StoryVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
        cell.viewWithTag(1)?.shadowToView()
        cell.viewWithTag(1)?.backgroundColor = .random()
        cell.lblDesc.text = arrDetails[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeDetailsVC") as! HomeDetailsVC
//        navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
