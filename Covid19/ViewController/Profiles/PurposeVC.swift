//
//  PurposeVC.swift
//  Covid19
//
//  Created by Apple on 04/03/21.
//

import UIKit

class PurposeVC: UIViewController {

    // MARK: - Variable
    
    // MARK: - IBoutlet
    @IBOutlet weak var containerview: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        
    }
    
    // MARK: - IBAction

    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: Any) {
        if containerview.isHidden == true {
            containerview.isHidden = false
            self.view.bringSubviewToFront(containerview)
        } else{
            containerview.isHidden = true
        }
    }

}
