//
//  ServicesVC.swift
//  Covid19
//
//  Created by Apple on 03/03/21.
//

import UIKit

class ServicesVC: UIViewController {

    // MARK: - Variable
    //var arrColour:[UIColor] = [#colorLiteral(red: 0.3019607843, green: 0.3019607843, blue: 0.3019607843, alpha: 1),#colorLiteral(red: 0, green: 0.631372549, blue: 0.368627451, alpha: 1),#colorLiteral(red: 0, green: 0.4823529412, blue: 0.8980392157, alpha: 1),#colorLiteral(red: 0.9843137255, green: 0.7137254902, blue: 0.1882352941, alpha: 1)]
    var arrServiceList:[ServicesListData]?
    
    // MARK: - IBoutlet
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblServices: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getServicesAPI(search: "")
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        table.tableFooterView = UIView()
        table.estimatedRowHeight = 140
        table.rowHeight = UITableView.automaticDimension
        
        
        lblServices.text = "Services".localizableString(lang: Helper.shared.lang)
        txtSearch.attributedPlaceholder = NSAttributedString(string:"Search".localizableString(lang: Helper.shared.lang), attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        
    }
    @objc func getHintsFromTextField(textField: UITextField) {
        print(textField.text)
        getServicesAPI(search: textField.text!)
        
    }
    //MARK: - Webservice
    func getServicesAPI(search: String) {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetServices(url: "\(URLs.get_services)\(search)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrServiceList = response.services?.data
                self.table.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    
    // MARK: - IBAction

    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: UIButton) {
//        if containerview.isHidden == true {
//            containerview.isHidden = false
//            self.view.bringSubviewToFront(containerview)
//        } else {
//            containerview.isHidden = true
//        }
        
        if sender.tag == 1 {
            heightView.constant = 0
            sender.tag = 2
        } else {
            sender.tag = 1
            heightView.constant = 50
        }
    }

}
extension ServicesVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrServiceList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServicesCell") as! ServicesCell
        cell.viewBg.backgroundColor = .random()
        cell.lblTitle.text = arrServiceList?[indexPath.row].title
        cell.lblDesc.text = arrServiceList?[indexPath.row].description
        
        cell.btnLearnmore.setTitle(NSLocalizedString("Learn more".localizableString(lang: Helper.shared.lang), comment: ""),for: .normal)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeDetailsVC") as! HomeDetailsVC
        obj.dictService = arrServiceList?[indexPath.row]
        obj.strFrom = "Service"
        navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
extension ServicesVC:UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        NSObject.cancelPreviousPerformRequests(
            withTarget: self,
            selector: #selector(self.getHintsFromTextField),
            object: textField)
        self.perform(
            #selector(self.getHintsFromTextField),
            with: textField,
            afterDelay: 1.0)
        return true
    }

}
