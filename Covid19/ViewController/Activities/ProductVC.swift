//
//  ProductVC.swift
//  Covid19
//
//  Created by Apple on 03/03/21.
//

import UIKit
import WebKit


class ProductVC: UIViewController {

    // MARK: - Variable
    var isFrom = ""
    
    // MARK: - IBoutlet
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var lblProduct: UILabel!
    @IBOutlet weak var collectionviewProduct: UICollectionView!
    @IBOutlet weak var collectionviewTshirt: UICollectionView!
    @IBOutlet weak var collectionviewComic: UICollectionView!
    @IBOutlet weak var webview: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        webview.navigationDelegate = self
        if isFrom == "Product" {
            webview.load(NSURLRequest(url: NSURL(string: Helper.shared.strProductUrl)! as URL) as URLRequest)
        } else if isFrom == "Comic" {
            webview.load(NSURLRequest(url: NSURL(string: Helper.shared.strComicUrl)! as URL) as URLRequest)
        }
        

    }
    
    // MARK: - IBAction

    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: Any) {
        if containerview.isHidden == true {
            containerview.isHidden = false
            self.view.bringSubviewToFront(containerview)
        } else{
            containerview.isHidden = true
        }
    }

}
extension ProductVC:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collectionviewProduct {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
            return cell
        }
        if collectionView == collectionviewTshirt {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubProductCell", for: indexPath) as! SubProductCell
            cell.viewWithTag(1)?.shadowToView()
            return cell
        }
        if collectionView == collectionviewComic {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubProductCell", for: indexPath) as! SubProductCell
            cell.viewWithTag(2)?.shadowToView()
            return cell
        }
        return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionviewProduct {
            
            return CGSize(width: self.view.frame.width-50, height: 128)
        }else if collectionView == collectionviewTshirt {
            
            return CGSize(width: (self.view.frame.width/2)-50, height: 250)
        } else {
            
            return CGSize(width: (self.view.frame.width/2)-50, height: 250)
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
        navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
extension ProductVC:WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        Helper.shared.showHUD()
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)  {
        Helper.shared.hideHUD()
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        
        Helper.shared.hideHUD()
    }
}
