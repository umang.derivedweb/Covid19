//
//  SeriesVC.swift
//  Covid19
//
//  Created by Apple on 05/03/21.
//

import UIKit

class SeriesVC: UIViewController {

    // MARK: - Variable
    var arrSeries:[SeriesListData]?
    
    // MARK: - IBoutlet
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var collectionview: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getSeriesAPI()
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        collectionview.register(UINib(nibName: "HeaderViewCV", bundle: nil), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderViewCV")
    }
    //MARK: - Webservice
    func getSeriesAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetSeries(url: "\(URLs.get_series)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrSeries = response.data?.data
                self.collectionview.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: Any) {
        if containerview.isHidden == true {
            containerview.isHidden = false
            self.view.bringSubviewToFront(containerview)
        } else{
            containerview.isHidden = true
        }
    }

}
extension SeriesVC:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrSeries?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return arrSeries?[section].series_videos?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IssuesCell", for: indexPath) as! IssuesCell
        cell.lblTitle.text = arrSeries?[indexPath.section].series_videos?[indexPath.row].title
        cell.lblDesc.text = arrSeries?[indexPath.section].series_videos?[indexPath.row].description
        cell.img.sd_setImage(with: URL(string: arrSeries?[indexPath.section].series_videos?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.width/2)-5, height: 260)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SeriesDetailVC") as! SeriesDetailVC
        obj.arrSeriesList = arrSeries?[indexPath.section].series_videos
        obj.index = indexPath.row
        navigationController?.pushViewController(obj, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderViewCV", for: indexPath) as! HeaderViewCV
            headerView.lblTitle.text = arrSeries?[indexPath.section].title
            headerView.backgroundColor = UIColor.clear
            return headerView
            
            
            
        default:
            assert(false, "Unexpected element kind")
            return UICollectionReusableView()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.collectionview.frame.width, height: 55)
    }
    
}
