//
//  CharacterVC.swift
//  Covid19
//
//  Created by Apple on 05/03/21.
//

import UIKit

class CharacterVC: UIViewController {

    // MARK: - Variable
    var arrCharacter:[CharacterListData]?
    
    // MARK: - IBoutlet
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getCharacterAPI()
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
        table.tableFooterView = UIView()
        table.estimatedRowHeight = 140
        table.rowHeight = UITableView.automaticDimension
    }
    //MARK: - Webservice
    func getCharacterAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetCharacter(url: "\(URLs.get_characters)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.arrCharacter = response.data?.data
                self.table.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: Any) {
        if containerview.isHidden == true {
            containerview.isHidden = false
            self.view.bringSubviewToFront(containerview)
        } else{
            containerview.isHidden = true
        }
    }

}
extension CharacterVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCharacter?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterCell") as! CharacterCell
        cell.viewWithTag(1)?.shadowToView()
        cell.lblTitle.text = arrCharacter?[indexPath.row].title
        cell.lblTitle1.text = arrCharacter?[indexPath.row].title1
        cell.lblTitle2.text = arrCharacter?[indexPath.row].title2
        cell.img.sd_setImage(with: URL(string: arrCharacter?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let obj = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeDetailsVC") as! HomeDetailsVC
//        navigationController?.pushViewController(obj, animated: true)
    }
    
    
}
