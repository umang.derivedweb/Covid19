//
//  AvatarVC.swift
//  Covid19
//
//  Created by Apple on 04/03/21.
//

import UIKit

class AvatarVC: UIViewController {

    // MARK: - Variable
    var arrAvatar:[AvatarListData]?
    
    // MARK: - IBoutlet
    @IBOutlet weak var containerview: UIView!
    @IBOutlet weak var collectionview: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        getAvatarAPI()
    }
    
    // MARK: - Function
    func setupUI() {
        
        navigationController?.navigationBar.isHidden = true
    }
    //MARK: - Webservice
    func getAvatarAPI() {
        Helper.shared.showHUD()
        
        let headers = ["Accept":"application/json"]
        
        NetworkManager.shared.webserviceCallGetAvatar(url: "\(URLs.get_avatars)", headers:headers) { (response) in
            
            if response.ResponseCode == 200 {
                
                self.view.viewWithTag(5)?.isHidden = false
                self.arrAvatar = response.data?.data
                self.collectionview.reloadData()
                
            } else {
                Toast.show(message: response.ResponseMsg ?? "", controller: self)
            }
            Helper.shared.hideHUD()
        }
    }
    // MARK: - IBAction

    @IBAction func btnMenu(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.centerContainer!.toggle(.left, animated: true, completion: nil)
    }
    @IBAction func btnSearch(_ sender: Any) {
        if containerview.isHidden == true {
            containerview.isHidden = false
            self.view.bringSubviewToFront(containerview)
        } else{
            containerview.isHidden = true
        }
    }

}
extension AvatarVC:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrAvatar?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvatarCell", for: indexPath) as! AvatarCell
        
        cell.lblTitle.text = arrAvatar?[indexPath.row].title
        cell.img.sd_setImage(with: URL(string: arrAvatar?[indexPath.row].image ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        if indexPath.row % 2 != 0 {
            
            cell.viewWithTag(1)?.isHidden = false
            cell.viewWithTag(2)?.isHidden = true
            cell.viewWithTag(3)?.isHidden = true
          } else {
            
            cell.viewWithTag(1)?.isHidden = true
            cell.viewWithTag(2)?.isHidden = false
            cell.viewWithTag(3)?.isHidden = false
          }
        
            
            
        if (arrAvatar?.count ?? 0) % 2 != 0 {
            
            if indexPath.row == arrAvatar!.count-1 {
                cell.viewWithTag(1)?.isHidden = true
                cell.viewWithTag(2)?.isHidden = true
            }
            
        } else {
            if indexPath.row == arrAvatar!.count-2 {
                cell.viewWithTag(1)?.isHidden = true
                cell.viewWithTag(2)?.isHidden = true
            }
            if indexPath.row == arrAvatar!.count-1 {
                cell.viewWithTag(1)?.isHidden = true
                cell.viewWithTag(2)?.isHidden = true
            }
            
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width/2, height: 180)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
    }
    
    
}
