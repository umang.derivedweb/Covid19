//
//  SeriesDetailVC.swift
//  Covid19
//
//  Created by Apple on 05/03/21.
//

import UIKit

class SeriesDetailVC: UIViewController {

    // MARK: - Variable
    
    var arrSeriesList:[SeriesVideosData]?
    var index:Int = 0
    
    // MARK: - IBOutlet
    @IBOutlet weak var collectionview: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    

    // MARK: - Function
    func setupUI() {
        collectionview.isPagingEnabled = false
        collectionview.scrollToItem(at: IndexPath(item: index , section: 0), at: .centeredHorizontally, animated: true)
        self.collectionview.setNeedsLayout()
        collectionview.isPagingEnabled = true
    }
    
    // MARK: - IBAction
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {

            let frame: CGRect = CGRect(x : contentOffset ,y : self.collectionview.contentOffset.y ,width : self.collectionview.frame.width,height : self.collectionview.frame.height)
            self.collectionview.scrollRectToVisible(frame, animated: true)
        }
    @IBAction func btnNext(_ sender: Any) {
        
        let collectionBounds = self.collectionview.bounds
                let contentOffset = CGFloat(floor(self.collectionview.contentOffset.x + collectionBounds.size.width))
                self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    @IBAction func btnPrevious(_ sender: Any) {
        let collectionBounds = self.collectionview.bounds
                let contentOffset = CGFloat(floor(self.collectionview.contentOffset.x - collectionBounds.size.width))
                self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    // MARK: -
    

}
extension SeriesDetailVC:UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return arrSeriesList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeriesDetailsCell", for: indexPath) as! SeriesDetailsCell
        cell.lblDesc.text = arrSeriesList?[indexPath.row].description
        cell.img.sd_setImage(with: URL(string: arrSeriesList?[indexPath.row].banner ?? ""), placeholderImage: UIImage(named: "placeholder.png"))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}
